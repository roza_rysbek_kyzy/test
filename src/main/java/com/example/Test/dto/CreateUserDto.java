package com.example.Test.dto;

import com.example.Test.enums.Role;
import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class CreateUserDto {
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String email;
    @NotNull
    private String password;
    @NotNull
    private Role role;
}