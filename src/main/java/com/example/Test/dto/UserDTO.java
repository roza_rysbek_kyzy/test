package com.example.Test.dto;

import com.example.Test.enums.Role;
import lombok.Data;

@Data
public class UserDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String username;

    private String password;

    private Role role;
}