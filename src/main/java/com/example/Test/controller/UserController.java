package com.example.Test.controller;

import com.example.Test.dto.CreateUserDto;
import com.example.Test.dto.UpdateUserDto;
import com.example.Test.dto.UserDTO;
import com.example.Test.dtoService.UserDTOService;
import com.example.Test.exception.UserSaveException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserDTOService userDTOService;

    @Autowired
    public UserController(UserDTOService userDTOService) {
        this.userDTOService = userDTOService;
    }

    @PostMapping("/create")
    public UserDTO create(@RequestBody CreateUserDto createUserDto) throws UserSaveException {
        return userDTOService.create(createUserDto);
    }

    @GetMapping("/{id}")
    public UserDTO getById(@PathVariable("id") Long id) {
        return userDTOService.getById(id);
    }

    @GetMapping
    public List<UserDTO> getAll() {
        return userDTOService.getAll();
    }

    @PutMapping
    public UserDTO update(@RequestBody UpdateUserDto updateUserDto) {
        return userDTOService.update(updateUserDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        userDTOService.delete(id);
    }
}