package com.example.Test.mapper;

import com.example.Test.dto.UserDTO;
import com.example.Test.entity.User;
import java.util.List;

public interface UserMapper {
    UserDTO toUserDTO(User user);
    List<UserDTO> toUserDTOList(List<User> all);
}