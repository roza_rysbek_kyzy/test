package com.example.Test.mapper.imp;

import com.example.Test.dto.UserDTO;
import com.example.Test.entity.User;
import com.example.Test.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserMapperImp implements UserMapper {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserMapperImp(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDTO toUserDTO(User user) {
        if (user == null) {
            return null;
        }

        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());

        return userDTO;
    }

    @Override
    public List<UserDTO> toUserDTOList(List<User> all) {
        if (all == null) {
            return null;
        }

        List<UserDTO> list = new ArrayList<>(all.size());
        for (User user : all) {
            list.add(toUserDTO(user));
        }

        return list;
    }
}