package com.example.Test.dtoService;

import com.example.Test.dto.CreateUserDto;
import com.example.Test.dto.UpdateUserDto;
import com.example.Test.dto.UserDTO;
import com.example.Test.exception.UserSaveException;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface UserDTOService {
    UserDTO create(CreateUserDto createUserDto) throws UserSaveException;
    UserDTO getById(Long id);
    List<UserDTO> getAll();
    UserDTO update(UpdateUserDto updateUserDto);
    void delete(Long id);
}