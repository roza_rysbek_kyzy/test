drop sequence if exists hibernate_sequence;
create sequence hibernate_sequence start 1 increment 1;
drop table if exists users cascade;
create table users
(
    id         bigserial NOT NULL ,
    email      varchar(255),
    first_name varchar(255),
    last_name  varchar(255),
    password   varchar(255),
    user_role  varchar(255),
    username   varchar(255),
    primary key (id)
);